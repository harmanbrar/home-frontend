import { Component, OnInit } from '@angular/core';
import { FreeApiService } from '../services/free-api.service';
import { Users } from '../classes/users';

@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.css']
})
export class ProductsComponent implements OnInit {

  constructor(private ApiData: FreeApiService) { }

  lstUsers: Users[];


  ngOnInit() {
    this.ApiData.getusers()
    .subscribe
    (
      data=>
      {
        this.lstUsers = data;
      }
    )
  }

}
