import { Component, OnInit } from '@angular/core';
import { Portfolio } from '../classes/portfolio';
import { FreeApiService } from '../services/free-api.service'

@Component({
  selector: 'app-portfolio',
  templateUrl: './portfolio.component.html',
  styleUrls: ['./portfolio.component.css']
})
export class PortfolioComponent implements OnInit {

  constructor(private ApiData: FreeApiService) { }

  lstPhotos = Portfolio[0];

  title = 'Portfolio';
  subTitle = 'Voluptua scripserit per ad, laudem viderer sit ex. Ex alia corrumpit voluptatibus usu, sed unum convenire id. Ut cum nisl moderatius, Per nihil dicant commodo an.';

  ngOnInit() {
    this.ApiData.getphotosbyparameter()
    .subscribe
    (
      data=>
      {
        this.lstPhotos = data;
      }
    )
  }

}
