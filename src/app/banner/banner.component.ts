import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-banner',
  templateUrl: './banner.component.html',
  styleUrls: ['./banner.component.css']
})
export class BannerComponent implements OnInit {

  constructor() { }

  title = 'Angular';
  subTitle = 'How to work with API';
  description = 'This is a demo theme in which we are using public API to test the frontend of website';
  btnValue = 'Get Started Now';
  logoSrc = 'assets/images/angular.png'

  ngOnInit() {
  }

}
