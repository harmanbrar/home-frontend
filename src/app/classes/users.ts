export class Users{
    
    id: number;
    name: string;
    email: string;
    address: string;
    phone: number;
    website: string;
    company: string;

}