import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient, HttpParams } from '@angular/common/http';

import { Team } from '../classes/team';



@Injectable({
  providedIn: 'root'
})
export class FreeApiService {

  constructor(private httpclient: HttpClient) { }

  getusers(): Observable<any>{
    return this.httpclient.get("https://jsonplaceholder.typicode.com/users");
  }

  getphotosbyparameter(): Observable<any>{
    let paramsPic = new HttpParams().set("albumId","99")
    return this.httpclient.get("https://jsonplaceholder.typicode.com/photos",{params:paramsPic});
  }

  post(newEmployee:Team): Observable<any>{
    return this.httpclient.post("https://reqres.in/api/users",newEmployee);
  }

  put(updateEmployee:Team): Observable<any>{
    return this.httpclient.put("https://reqres.in/api/users/2",updateEmployee);
  }
  patch(updateEmployee:Team): Observable<any>{
    console.log('************************', updateEmployee)
    return this.httpclient.patch("https://reqres.in/api/users/2",updateEmployee);
  }

  delete(): Observable<any>{
    return this.httpclient.delete("https://reqres.in/api/users/2");
  }
}
