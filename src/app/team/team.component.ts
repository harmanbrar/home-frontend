import { Component, OnInit } from '@angular/core';
import { FreeApiService } from '../services/free-api.service';
import { Team } from '../classes/team'

@Component({
  selector: 'app-team',
  templateUrl: './team.component.html',
  styleUrls: ['./team.component.css']
})
export class TeamComponent implements OnInit {

  constructor(private ApiData: FreeApiService) { }

  addUser: Team;
  updatedUser: Team;
  patchedUser: Team;
  message: string;

  ngOnInit() {
    var createUser = new Team();
    createUser.email = 'harmank.dz@gmail.com';
    createUser.first_name = 'Harman';
    createUser.last_name = 'Brar';
    createUser.avatar = 'https://s3.amazonaws.com/uifaces/faces/twitter/calebogden/128.jpg';

    this.ApiData.post(createUser)
    .subscribe
    (
      data=>
      {
       this.addUser = data;
      }
    );

     createUser = new Team();
     createUser.first_name = 'Harman Kaur';
     createUser.last_name = 'Brar';
     createUser.email = 'harmanbrar@gmail.com';
     createUser.avatar = 'https://s3.amazonaws.com/uifaces/faces/twitter/josephstein/128.jpg';

     this.ApiData.put(createUser)
    .subscribe
    (
      data=>
      {
       this.updatedUser = data;
      }
    );

    createUser = new Team();
    createUser.first_name = 'Record Patched';
     
     this.ApiData.patch(createUser)
    .subscribe
    (
      data=>
      {
       this.patchedUser = data;
       console.log('---------------------', data)
      }
    );

    this.ApiData.delete()
    .subscribe
    (
      data=>
      {
        this.message = "Record Deleted";
      }
    );
  }
}
